import mcpi.minecraft as minecraft
import mcpi.block as block
import mcpi.event
from time import sleep
from math import *

mc = minecraft.Minecraft.create()

#Main function
def main():

    #Define the location of the TARDIS' "interior"
    #relative to spawn point.
    inside_x = 0
    inside_y = 0
    inside_z = 0

    #Define the initial coordinates of the TARDIS
    tardis_x = 0
    tardis_y = 0
    tardis_z = 0

    #Define the location of the TARDIS' control block.
    control_x = 0
    control_y = 1
    control_z = 0

    #Define the location of the TARDIS location block.
    activationSwitch_x = 3
    activationSwitch_y = 1
    activationSwitch_z = 3

    tardis_spawned = False

    #Build the "Interior" of the TARDIS.
    #Not actually bigger on the inside.
    build_room(-5,0,-5, 5,5,5, block.GOLD_BLOCK.id)

    #Build the block used to set the coordinates.
    mc.setBlock(control_x, control_y, control_z, block.GLOWSTONE_BLOCK.id,0)

    #Build the block used to make the TARDIS appear and dissapear.
    mc.setBlock(activationSwitch_x, activationSwitch_y, activationSwitch_z, block.WOOL, 14)

    #Build the "door" that lets the user enter and exit the TARDIS
    mc.setBlocks(0,1,-5, 0, 2, -5, block.IRON_BLOCK, 0)

    #Start the main loop.
    while True:
        #Poll for block events since the last iteration of the loop.
        blockEvents = mc.events.pollBlockHits()
        for hitBlock in blockEvents:

            #Activation block:
            #Makes the TARDIS appear or dissapear.

            if (
            mc.getBlock(hitBlock.pos.x, hitBlock.pos.y, hitBlock.pos.z) == block.WOOL.id
            and
            hitBlock.pos.x == activationSwitch_x and
            hitBlock.pos.y == activationSwitch_y and
            hitBlock.pos.z == activationSwitch_z):
                if(tardis_spawned):
                    tardis_spawned = False
                    erase_tardis(tardis_x - 1, tardis_y, tardis_z - 1)
                    mc.setBlock(activationSwitch_x, activationSwitch_y, activationSwitch_z, block.WOOL,14)
                    mc.postToChat("The TARDIS has left.")
                else:
                    tardis_y = mc.getHeight(tardis_x,tardis_z) + 1
                    if build_tardis(tardis_x - 1, tardis_y, tardis_z - 1):
                        tardis_spawned = True
                        mc.postToChat("The TARDIS has landed.")
                        mc.setBlock(activationSwitch_x, activationSwitch_y, activationSwitch_z, block.WOOL,13)
                    else:
                        mc.postToChat("ERROR! This location is blocked!")

            #Navigation Block:
            #Pressing a face on the block sets the coordinates that the TARDIS
            #will appear at when activated.
            if (
            mc.getBlock(hitBlock.pos.x, hitBlock.pos.y, hitBlock.pos.z) == block.GLOWSTONE_BLOCK.id
            and
            hitBlock.pos.x == control_x and
            hitBlock.pos.y == control_y and
            hitBlock.pos.z == control_z):
                if(tardis_spawned):
                    mc.postToChat("ERROR! Tardis cannot be moved while it has landed.")
                else:
                    #if (hitBlock.face == 1): tardis_y += 1
                    if (hitBlock.face == 2): tardis_x += 1
                    if (hitBlock.face == 4): tardis_z += 1
                    if (hitBlock.face == 3): tardis_x += -1
                    if (hitBlock.face == 5): tardis_z += -1
                    #if (hitBlock.face == 0): tardis_y += 1
                    tardis_y = mc.getHeight(tardis_x,tardis_z) + 1

                    mc.postToChat("Coordinates: (" + str(tardis_x) + ", " + str(tardis_y) + ", " + str(tardis_z) +")")

            #"Door" block:
            #Teleports the user to the TARDIS, if it has spawned.
            if (mc.getBlock(hitBlock.pos.x, hitBlock.pos.y, hitBlock.pos.z) == block.IRON_BLOCK.id):
                if (tardis_spawned == False):
                    mc.postToChat("Warning: TARDIS is presently in the Time Vortex. Leaving is not advisable.")
                else:
                    entityIds = mc.getPlayerEntityIds()
                    blockEventPlayer = hitBlock.entityId
                    mc.entity.setPos(blockEventPlayer,tardis_x,tardis_y,tardis_z)

            #TARDIS "Door" block:
            #Teleports user back to the interior when used.
            if (
            mc.getBlock(hitBlock.pos.x, hitBlock.pos.y, hitBlock.pos.z) == block.WOOL.id
            and
            hitBlock.pos.x == tardis_x and
            hitBlock.pos.y == tardis_y and
            hitBlock.pos.z == tardis_z and
            tardis_spawned == True):
                mc.entity.setPos(blockEventPlayer, 0, 2, -4)

        #Wait for a split-second before looping (to conserve memory)
        sleep(0.5)

#---------------- Utility Functions Below This Line -----------


def build_room(x, y, z, width, height, depth, block_type):
    #Builds a 1-block thick room out of whatever material you choose.
    mc.setBlocks(x, y, z, width, height, depth, block_type,0)
    mc.setBlocks(x + 1, y + 1, z + 1, width-1, height-1, depth-1, block.AIR,0)


def build_tardis(x, y, z):
    #Checks to see if you can build a TARDIS in the specified location, and then builds it.
    #Returns True if sucessful, False if blocked.
    width = 2
    depth = 2
    height = 2

    #---------------------------WARNING------------------------
    #The commented line of code does NOT work on the Raspberry Pi

    #checkBlocks = mc.getBlocks(x , y - 1, z, x + width, y + height, z + depth)

    #----------------------------------------------------------

    checkBlocks = []

    for i in range(x, x + width + 1):
        for j in range(y - 1, y + height + 1):
            for k in range(z, z + depth + 1):
                    checkBlocks.append (mc.getBlock(i,j,k))

    for blk in checkBlocks:
        if blk != block.AIR.id: return False


    mc.setBlocks(x , y - 1, z, x + width, y + height, z + depth, block.LAPIS_LAZULI_BLOCK,0)

    mc.setBlocks(x+1, y, z, x+1, y + 1, z+1, block.AIR,0)
    mc.setBlocks(x+1, y, z+1, x+1, y + 1, z+1, block.WOOL,0)

    return True

def erase_tardis(x,y,z):
    #Erases the TARDIS in the specifed location.
    #It's not actually hard-coded to erase only the TARDIS, so be careful with this.
    width = 2
    depth = 2
    height = 3

    mc.setBlocks(x , y - 1, z, x + width, y + height, z + depth, block.AIR,0)


main()

